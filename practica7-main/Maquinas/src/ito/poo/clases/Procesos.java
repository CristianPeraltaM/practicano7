package ito.poo.clases;

import java.time.LocalDate;

public class Procesos {
	private String descripcion ;
	private LocalDate Fecha_adquisicion;
	private float costoXmaquina;
	
public Procesos(String descripcion, LocalDate fecha_adquisicion, float costoXmaquina) {
	
		this.descripcion = descripcion;
		Fecha_adquisicion = fecha_adquisicion;
		this.costoXmaquina = costoXmaquina;
	}

public String getDescripcion() {
	return descripcion;
}

public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
}

public float getCostoXmaquina() {
	return costoXmaquina;
}

public void setCostoXmaquina(float costoXmaquina) {
	this.costoXmaquina = costoXmaquina;
}

public LocalDate getFecha_adquisicion() {
	return Fecha_adquisicion;
}

@Override
public String toString() {
	return "Procesos [descripcion=" + descripcion + ", Fecha_adquisicion=" + Fecha_adquisicion + ", costoXmaquina="
			+ costoXmaquina + "]";
}

}
